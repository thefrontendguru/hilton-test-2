import React, { Component } from "react";
import Rooms from "./components/Rooms/Rooms";
import "./styles/main.less";
class App extends Component {
  render() {
    return (
      <Rooms />
    );
  }
}

export default App;