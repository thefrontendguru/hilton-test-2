import React, { Component } from "react";
import PropTypes from "prop-types";
import Room from "../Room/Room";
import { connect } from "react-redux";
import "./rooms.less";

const LOCAL_STORED_ROOMS_KEY = "roomInfo";
class Rooms extends Component {
  state = {
    isSaved: false,
    roomsState: [
      {
        roomNumber: 1,
        adultCount: 1,
        childCount: 0,
        isActive: true
      },
      {
        roomNumber: 2,
        adultCount: 0,
        childCount: 0,
        isActive: false
      },
      {
        roomNumber: 3,
        adultCount: 0,
        childCount: 0,
        isActive: false
      },
      {
        roomNumber: 4,
        adultCount: 0,
        childCount: 0,
        isActive: false
      },
    ]
  }

  componentDidMount() {
    const localState = JSON.parse(localStorage.getItem(LOCAL_STORED_ROOMS_KEY));
    if (!!localState) this.setState({ roomsState: localState });
  }

  render() {
    const { roomsState, isSaved } = this.state;
    return (
      <div className="rooms">
        <div className="rooms--room-container">
          {roomsState.map((room, idx) => {
            const { roomNumber, adultCount, childCount, isActive } = room;
            const currentRoomNumber = roomNumber - 1;
            return (
              <Room
                key={idx}
                roomNumber={roomNumber}
                hideCheckbox={roomNumber == 1}
                checked={roomsState[currentRoomNumber].isActive}
                onCheckboxChange={() => this.handleCheckboxChange(idx)}
                onAdultDropdownChange={(e) => this.handleAdultDropdownChange(e, idx)}
                onChildDropdownChange={(e) => this.handleChildDropdownChange(e, idx)}
                dropDownAdultVal={roomsState[idx].adultCount}
                dropDownChildVal={roomsState[idx].childCount}
              />
            );
          })}
        </div>
        <div className="rooms--submit-container">
          <button
            className="rooms--submit-button"
            disabled={this.state.isSaved}
            type="submit"
            onClick={this.handleSubmit}
          >
            {isSaved ? "...saving" : "Submit"}
          </button>
        </div>
      </div>
    );
  }

  handleCheckboxChange = (idx) => {
    const { roomsState } = this.state;
    const newRoomState = [...roomsState];
    if (idx === 3) {
      if (!roomsState[idx].isActive) {
        newRoomState[idx].isActive = true;
        newRoomState[idx - 1].isActive = true;
        newRoomState[idx - 2].isActive = true;
      } else if (roomsState[idx].isActive) {
        newRoomState[idx].isActive = false;
        newRoomState[idx - 1].isActive = false;
        newRoomState[idx - 2].isActive = false;
      }
    } else if (idx === 2) {
      if (!roomsState[idx].isActive) {
        newRoomState[idx].isActive = true;
        newRoomState[idx - 1].isActive = true;
      } else if (roomsState[idx].isActive) {
        newRoomState[idx].isActive = false;
        newRoomState[idx - 1].isActive = false;
        newRoomState[idx + 1].isActive = false;
      }
    } else {
      newRoomState[idx].isActive = !roomsState[idx].isActive;
      newRoomState[idx + 1].isActive = false;
      newRoomState[idx + 2].isActive = false;
    }

    this.setState({
      roomsState: newRoomState
    });
  }

  handleAdultDropdownChange = (e, idx) => {
    const { roomsState } = this.state;
    const newRoomState = [...roomsState];
    newRoomState[idx].adultCount = e * 1;

    this.setState({
      roomsState: newRoomState
    });
  }
  handleChildDropdownChange = (e, idx) => {
    const { roomsState } = this.state;
    const newRoomState = [...roomsState];
    newRoomState[idx].childCount = e * 1;

    this.setState({
      roomsState: newRoomState
    });
  }
  handleSubmit = () => {
    const { roomsState } = this.state;

    localStorage.setItem(LOCAL_STORED_ROOMS_KEY, JSON.stringify(roomsState));
    this.setState({ isSaved: true });
    setTimeout(() => this.setState({ isSaved: false }), 1000);
  }
}

Rooms.propTypes = {
  count: PropTypes.number
};

const mapStateToProps = (store) => {
  return {
    count: store
  };
};

export default connect(mapStateToProps)(Rooms);
