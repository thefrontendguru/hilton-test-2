import React, { Component } from "react";
import PropTypes from "prop-types";
import "./room.less";

class Room extends Component {

  static get propTypes() {
    return {
      roomNumber: PropTypes.number,
      dropDownAdultVal: PropTypes.number,
      dropDownChildVal: PropTypes.number,
      dropDownAdult: PropTypes.array,
      dropDownChild: PropTypes.array,
      isActive: PropTypes.bool,
      hideCheckbox: PropTypes.bool,
      checked: PropTypes.bool,
      onCheckboxChange: PropTypes.func,
      onAdultDropdownChange: PropTypes.func,
      onChildDropdownChange: PropTypes.func
    };
  }

  static get defaultProps() {
    return {
      checked: false,
      roomNumber: 0,
      dropDownAdult: [1, 2],
      dropDownChild: [0, 1, 2],
    };
  }

  render() {
    const {
      roomNumber,
      dropDownAdult,
      dropDownChild,
      hideCheckbox,
      checked,
      onChildDropdownChange,
      onAdultDropdownChange,
      dropDownAdultVal,
      dropDownChildVal
    } = this.props;
    const disabledCls = checked ? "" : "disabled";
    const isisActive = hideCheckbox ? false : !checked;
    return (
      <div className={`room ${disabledCls}`}>
        <div className="room--header">
          {!hideCheckbox && <input
            className="room--header-checkbox"
            type="checkbox"
            name={`roomNumber${roomNumber}`}
            onChange={this.handleCheck}
            checked={checked}
          />}
          <h4 className="room--header-title">Room {roomNumber}</h4>
        </div>
        <div className="room--body">
          <div className="room--selection-container">
            <label className="room--checkbox-label">Adults (18+)</label>
            <select value={dropDownAdultVal} disabled={isisActive} onChange={this.handleAdultDropdownChange}>
              {this.renderOptions(dropDownAdult)}
            </select>
          </div>
          <div className="room--selection-container">
            <label className="room--checkbox-label">Children (0-17)</label>
            <select value={dropDownChildVal} disabled={isisActive} onChange={this.handleChildDropdownChange}>
              {this.renderOptions(dropDownChild)}
            </select>
          </div>
        </div>

      </div>
    );
  }
  renderOptions = (arr) => {
    return arr.map((num) => <option key={num} value={num}>{num}</option>);
  }
  handleCheck = () => {
    this.props.onCheckboxChange();
  }
  handleAdultDropdownChange = (e) => {
    this.props.onAdultDropdownChange(e.target.value);
  }
  handleChildDropdownChange = (e) => {
    this.props.onChildDropdownChange(e.target.value);
  }
}

export default Room;
