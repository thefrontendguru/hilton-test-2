# Hilton Test # 2

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
In your terminal, in the root folder of this file, run the following commands
```
npm i
npm run start:dev
```
or if you are using yarn
```
yarn
yarn start:dev
```


## Explanations

### Styled Components
You will probably look over me becuase i didnt use styled components. If you are still reading after that sentence, let me tell you why:
I worked with styled components on a few enterprise projects. There was a concerted effort to swtich after using scss/less becuase of style breakage. 
Upon moving to style comps, new problems were created that ultimatley had us move back to scss/less. In my experience, working on enterprise 
applications, scss/less has been the most scalable style language when used correctly (emphasis on correctly). And 95% percent of the time, the problem is name spacing and a 
 general lack of understanding of semantic DOM (HTML), and scalable css (like flexbox). When implemented correctly, the implications were huge when considering 
 right to left, multiple browser compatiabilty, and incorporating responsive design on a massive level. Those are my 2 shiny pennies and can be better explained here: http://jamesknelson.com/why-you-shouldnt-style-with-javascript/.

### Persisting Data / Redux
I was going to use redux at first, but as i continued, i noticed that it wasnt necesarry (you may see an actions and reducers folder). So instead of using the redux store, i used the state of the compoent. 
I could just as easily add redux thunk middle ware, create some actions and reducers, but for the size of this project, it seemed like overkill. 

Also, I am persisting data using local storage. I contemplated using firebase, or some quick database i could add to the project, but, again...overkill.



